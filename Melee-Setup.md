## Asheron's Call - Gauntlet Prep Checklist - Melee

Tips for a successful Melee Character in a Gauntlet Run: Checklist of Requirements and Strong Recommendations.


Note: While Two-Handed combat does comparable if not better damage than archers but other melee weapon masteries will struggle DPS-wise on Gauntlet.

Recommended Virindi Tank configuration can be found at the bottom of this document.


### Required Base:
- [ ] Level 275
- [ ] All XP Augmentations
- [ ] Fully Lum Augmented at Nalicana and Seer of Choice
- [ ] 460+ Buffed Health
- [ ] 35+ Damage Rating 
- [ ] 15+ Crit Damage Rating
- [ ] Full Legendary Suit, Including Legendary Life Magic
- [ ] A way of quickly burning Vit if you die 
  - [ ] Being lifestoned at DI and 10+ Corrupted Essence in inventory is the recommended method.
- [ ] Curse of Raven Fury (Tugak)
- [ ] Incantation of Marty's Hecatomb
  - [ ] Learned via Life Magic Certificate (Obtained via Stipends)

### Required items:
- [ ] Luminous Crystal of Rare Armor Damage Boost V (Or Rare Armor)
- [ ] 10+ Spectral Crystals (For your weapon mastery)
- [ ] 10+ Spectral Dual Wield Mastery Crystals (If Dual Wield Spec)
- [ ] 10+ Spectral Crystal of the Life Giver

### Required Weapons:
- Quest Weapons
  - [ ] Tusker Paw Wand
  - [ ] Life-attuned Shadowfire Isparian Wand
  - [ ] Burun Hunters Martyr Scepter (Requires 385 buffed Life Magic)
  - [ ] Rynthid weapon
  - [ ] Soul Bound weapon
  - [ ] Grievver weapon
- Tinkered (Elemental Rend)
  - [ ] Bludgeoning
  - [ ] Pierce (Mukkir Slayer)
  - [ ] Fire (Undead Slayer)
  - [ ] Frost
- Tinkered (Armor Rending)
  - [ ] Acid
  - [ ] Slash
  - [ ] Pierce
  

### Recommended:
- [ ] 40+ Damage Rating
- [ ] 18+ Crit Damage Rating
- [ ] Summoning
- [ ] Luminous Crystal of Rare Damage Reduction V
- [ ] Burning Coal / Jaria's Burning Coal
- [ ] Equipment that gives Warrior's Vitality
- [ ] 30+ Mushroom Jerky
- [ ] Tusker Stout Beer (10+ For the following attributes)
  - [ ] Strength
  - [ ] Endurance
  - [ ] Coordination
  - [ ] Quickness
  - [ ] Focus
  - [ ] Self
- [ ] Luminous Crystal of Vitality
- [ ] Endless Mucor Fungus
- [ ] Endless Macran'oni and Cheese
- [ ] Hunter's Healing Salve
- [ ] Lesser Stamina Kits
- [ ] MR Hunter Summon (Presuming your summoning skill is high enough)
- [ ] 10+ Hunter's Mead
- [ ] Honeyd Mead (Life,Vigor,Mana)
- [ ] Essence of Cave Penguin



### VTank Settings
<div>
  <p>Melee VTank Options(Main) Tab</p>
  <img src="Images/Melee_VTank-MainTab.png" width="600">
</div>
<div>
  <p>Melee VTank Vitals Tab</p>
  <img src="Images/Melee_Vtank-VitalsTab.png" width="600">
</div>
<div>
  <p>Melee VTank Consumables Tab</p>
  <img src="Images/Melee-VTank-ConsumablesTab.png" width="600">
</div>

Weapon Types for Monsters
- Corrosive Archer, Soul Bound Weapon
- Virindi Rival, Rynthid Tentacle Weapon
- Tumerok Savage, Pierce AR
- Lugian Launcher, Grievver Infused Weapon
- Torment Wisp, Slash AR
- Vicious Remoran Sapper, Bludgeoning Rend
- Pike Grievver, Grievver Infused Weapon
- Grievver Darter, Grievver Infused Weapon
- Vibrant Shadow,  Life-Attuned Shadowfire Isparian Wand
- Mukkir Predator, Pierce (Mukkir Slayer)
- Ruuk Ranger, Burun Hunters Martyr Scepter
- Guruk Grunt, Burun Hunters Martyr Scepter
- Crazed Olthoi, Pierce AR
- Tundra Tusker, Tusker Paw Wand
- Defensive Crystal, Fire Rend
- Stinging Armoredillo, Slash AR
- Wight Sage, Fire Rend Undead Slayer
- Blade Lieutenant, Acid AR
- Blade Captain, Acid AR
- Blade Champion, Acid AR
- Tusker Heaver, Fire Rend


<div>
  <p>Melee VTank Monster Tab</p>
  <img src="Images/Melee_VTank-MonstersTab.png" width="600">
</div>