### Gauntlet Arena Meta Setup
-------

This guide covers the process for setting up the Asheron's Call Gauntlet Meta, available at the following link.<br>
https://www.immortalbob.com/phpBB3/viewtopic.php?f=6&t=669

This meta is very useful when you want to run multiple characters you control through Gauntlet at once as it has many useful features.
- Missile accuracy slider dynamically set based on character and missile skill
    - Optimal DPS aims for 97% hit rate and 120 points above target missile defense
- Spectrals used automatically in arenas, unless Life Spectrals specified for vulners
- Stage 5 set up to recall and return to Gauntlet after Pike Grievvers killed
- DI Lifestoned characters with DI trophies attempt to turn one in before returning to Gauntlet
- Deck of Eyes and Black Luster Pearls used in Stage 10 for Armoredillos
- Use #report to display stage times after run completion
- Checks in place to catch stuck characters, but monitor group for issues.


Pre-Requisites:
- [Virindi Tank](http://www.virindi.net/wiki/index.php/Virindi_Tank)
- [Utility Belt 0.2.7+](https://utilitybelt.gitlab.io/)
- [Chaos Helper](https://www.immortalbob.com/phpBB3/viewtopic.php?t=282)


Setup Steps (Will expand upon this and the pre-reqs in details later)
- [Download the Meta Files here](https://www.immortalbob.com/phpBB3/viewtopic.php?f=6&t=669)
- Open the downloaded .zip file and extract it to your Virindi Tank folder
  - This is usually ```C:\Games\VirindiPlugins\VirindiTank```
  - Copy these two files to your Chaos Helper folder
    - Gauntlet.Layout
    - Gauntlet.txt
- Load the meta file in VTank (You might have to type /vt refresh in order to see if you had the game running)
- When in the Gauntlet Lobby, load the Meta, your Gauntlet VTank Profile.
- Once you've loaded the meta there should be a custom Chaos Helper profile that allows you to control your character.
- <div>
  <p>Chaos Helper Menu</p>
  <img src="Images/Meta-Remote.png">
</div>
- Once it sees the fellowship is locked message, the meta will use the Arena Portal and then begin to run through each stage as they are cleared.
- If you Die and are Lifestoned at DI, it will hand in Corrupted Essence to Burn Vitae, Portal Recall then run back to the Arena and continue fighting.



## License

This repository is licensed under the [MIT License](https://opensource.org/licenses/MIT).