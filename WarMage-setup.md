## Asheron's Call - Gauntlet Prep Checklist - War Mage

Tips for a Successful War Mage Character in a Gauntlet Run: Checklist of Presumed Requirements and Strong Recommendations.

Recommended Virindi Tank configuration can be found at the bottom of this document.


### Required Base:
- [ ] Level 275
- [ ] All XP Augmentations
- [ ] Fully Lum Augmented at Nalicana and Seer of Choice
- [ ] 460+ Buffed Health
- [ ] 35+ Damage Rating 
- [ ] 15+ Crit Damage Rating
- [ ] Full Legendary Suit, Including Legendary Life Magic
- [ ] Life Magic Spec*
  - [ ] This can be worked around by having 680+ Buffed Life in addition to Using ED Focus or Willpower beer for the Dillos and Olthoi.
- [ ] A way of quickly burning Vit if you die 
  - [ ] Being lifestoned at DI and 10+ Corrupted Essence in inventory is the recommended method.
- [ ] Curse of Raven Fury (Tugak)
- [ ] Incantation of Marty's Hecatomb
  - [ ] Learned via Life Magic Certificate (Obtained via Stipends)

### Required items:
- [ ] Luminous Crystal of Rare Armor Damage Boost V (Or Rare Armor)
- [ ] 10+ Spectral Crystal of the Hieromancer
- [ ] 10+ Spectral Crystal of the Life Giver

### Required Weapons:
- Quest Weapons
  - [ ] Tusker Paw Wand
  - [ ] Shadowfire Isparian Wand
  - [ ] Burun Hunters Martyr Scepter (Requires 385 buffed Life Magic)
  - [ ] Rynthid Tentacle Wand
  - [ ] Soul Bound Staff
  - [ ] Paradox-touched Olthoi Wand (War)
  - [ ] Enhanced Assault Orb
  - [ ] Grievver Infused War Wand
- Tinkered (Elemental Rend)
  - [ ] Pierce (Mukkir Slayer)
  - [ ] Fire (Undead Slayer)
  - [ ] Frost
- Tinkered (Critical Strike)
  - [ ] Acid
  - [ ] Slash
  - [ ] Bludgeon

### Recommended:
- [ ] 5 Adept / 4 Defender Armor Set
  - [ ] If using rare equip 5 Adept / 3 Def
- [ ] 40+ Damage Rating
- [ ] 20+ Crit Damage Rating
- [ ] Summoning Spec
- [ ] Luminous Crystal of Rare Damage Reduction V
- [ ] Burning Coal / Jaria Burning Coal
- [ ] Equipment that gives Warrior's Vitality
- [ ] 30+ Mushroom Jerky
- [ ] Tusker Stout Beer (10+ For these attributes)
  - [ ] Focus
  - [ ] Self
  - [ ] Endurance
- [ ] Desk of Eyes
- [ ] Luminous Crystal of Vitality
- [ ] Endless Mucor Fungus
- [ ] Endless Macran'oni and Cheese
- [ ] Hunter's Healing Salve
- [ ] Lesser Stamina Kits
- [ ] Black Luster Pearl
- [ ] MR Hunter Summon
- [ ] 10+ Hunter's Mead
- [ ] Honeyd Mead (Life,Vigor,Mana)
- [ ] Essence of Cave Penguin



### VTank Settings
<div>
  <p>War Mage VTank Options(Main) Tab</p>
  <img src="Images/WarMage_VTank-MainTab.png" width="600">
</div>
<div>
  <p>War Mage VTank Vitals Tab</p>
  <img src="Images/WarMage_VTank-VitalsTab.png" width="600">
</div>
<div>
  <p>War Mage VTank Consumables Tab</p>
  <img src="Images/WarMage_VTank-ConsumablesTab.png" width="600">
</div>

Weapon Types for Monsters
- Corrosive Archer, Soul Bound Staff
- Virindi Rival, Rynthid Tentacle Wand
- Tumerok Savage, Enhanced Assault Orb
- Lugian Launcher, Grievver Infused Weapon
- Torment Wisp, Slash CS
- Vicious Remoran Sapper, Blunt CS
- Pike Grievver, Grievver Infused War Wand
- Grievver Darter, Grievver Infused War Wand
- Vibrant Shadow, Shadowfire Isparian Wand
- Mukkir Predator, Pierce Rend Mukkir Slayer
- Ruuk Ranger, Burun Hunters Martyr Scepter
- Guruk Grunt, Burun Hunters Martyr Scepter
- Crazed Olthoi, Paradox-touched Olthoi Wand
- Tundra Tusker, Tusker Paw Wand
- Defensive Crystal, Fire Rend
- Stinging Armoredillo, Slash CS
- Wight Sage, Fire Rend Undead Slayer
- Blade Lieutenant, Acid CS
- Blade Captain, Acid CS
- Blade Champion, Acid CS
- Tusker Heaver, Fire Rend


<div>
  <p>War Mage VTank Monster Tab</p>
  <img src="Images/WarMage_VTank-MonsterTab.png" width="600">
</div>


<div>
  <p>Advanced Options</p>
    <p>DebuffEachFirst: All</p>
  <img src="Images/WarMage_VTank-ADV-SpellCombat.png" width="400">
</div>

### Comments:
War mages are a nuanced class on Gauntlet runs. There are some monsters they can do a large amount of damage to but do struggle in multiple rooms due to elemental and magic resistance of some monsters. For this reason, it is presumed that Gauntlet was engineered in a way that there would be very few War Mages on a run, and those whom are would have Life Magic Spec so that they can provide Imp and Vuln support.

When testing with 520 health and 39D, Hecatomb and Raven Fury were vastly better then Void magic, often doing 3 to 4 times the amount of damage of void. 
- For example, a Void ring will hit a tundra tusker for 650-900 but Raven Fury will consistently hit for 1800.
  - This is a pretty substantial improvement even with the drawback that it takes about twice as long to cast because you have to heal between casts.
- This damage scales up even higher with Honeyed Life Mead and Or Cave Penguin Essence buffs active.