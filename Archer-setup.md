## Asheron's Call - Gauntlet Prep Checklist - Archer

Tips for a Successful Missile Character in a Gauntlet Run: Checklist of Presumed Requirements and Strong Recommendations.

Recommended Virindi Tank configuration can be found at the bottom of this document.


### Required Base:
- [ ] Level 275
- [ ] All XP Augmentations
- [ ] Fully Lum Augmented at Nalicana and Seer of Choice
- [ ] 460+ Buffed Health
- [ ] 35+ Damage Rating 
- [ ] 15+ Crit Damage Rating
- [ ] Full Legendary Suit, Including Legendary Life Magic, Focus, Willpower
- [ ] A way of quickly burning Vit if you die 
  - [ ] Being lifestoned at DI and 10+ Corrupted Essence in inventory is the recommended method.
- [ ] Curse of Raven Fury (Tugak)
- [ ] Incantation of Marty's Hecatomb
  - [ ] Learned via Life Magic Certificate (Obtained via Stipends)

### Required items:
- [ ] Luminous Crystal of Rare Armor Damage Boost V (Or Rare Armor)
- [ ] Deadly Prsimatic Arrows or Quarrels
- [ ] 10+ Spectral Missile Weapon Mastery Crystal
- [ ] 10+ Spectral Crystal of the Life Giver

### Required Weapons:
- Quest:
  - [ ] Tusker Paw Wand
  - [ ] Life-attuned Shadowfire Isparian Wand
  - [ ] Burun Hunters Martyr Scepter (Requires 385 buffed Life Magic)
  - [ ] Rynthid Tentacle Bow
  - [ ] Soul Bound Bow
  - [ ] Paradox-touched Olthoi Bow
  - [ ] Enhanced Assault Bow (AR Piece bow can temporarily substitute)
  - [ ] Grievver Infused (Bow or Crossbow)
- Tinkered (Elemental Rend):
  - [ ] Bludgeon
  - [ ] Pierce (Mukkir Slayer)
  - [ ] Fire (Undead Slayer)
  - [ ] Frost
- Tinkered (Armor Rending):
  - [ ] Acid
  - [ ] Slash
  

### Recommended:
- [ ] 5 Dex / 4 Defender Armor Set
  - [ ] If using rare equip 5 Dex / 3 Def
- [ ] 40+ Damage Rating
- [ ] 20+ Crit Damage Rating
- [ ] Equipment that gives Warrior's Vitality
- [ ] Burning Coal / Jaria Burning Coal
- [ ] Tusker Stout Beer (10+ For the following attributes)
  - [ ] Focus
  - [ ] Self
  - [ ] Endurance
  - [ ] Coordination
- [ ] Luminous Crystal of Vitality
- [ ] Endless Mucor Fungus
- [ ] Endless Macran'oni and Cheese
- [ ] 30+ Mushroom Jerky
- [ ] Hunter's Healing Salve
- [ ] Lesser Stamina Kits
- [ ] 10+ Hunter's Mead
- [ ] Cave Penguin Essence
- [ ] Summoning and Sneak Spec
  - [ ] Deception Trained
- [ ] Luminous Crystal of Rare Damage Reduction V (Or Rare Armor)
- [ ] MR Hunter Summon
- [ ] Diamond Golem Summon



### VTank Settings

<div>
  <p>Archer VTank Options(Main) Tab</p>
  <img src="Images/Archer_VTank-MainTab.png" width="600">
</div>
<div>
  <p>Archer VTank Vitals Tab</p>
  <img src="Images/Archer_VTank-VitalsTab.png" width="600">
</div>
<div>
  <p>Archer VTank Consumable Tab</p>
  <img src="Images/Archer_VTank-ConsumableTab.png" width="600">
</div>


Weapon Types for Monsters
- Hollow Pawn, Frost Rend
- Corrosive Archer, Soul Bound Bow
- Virindi Rival, Rynthid Tentacle Bow
- Tumerok Savage, Enhanced Assault Bow (Or Pierce AR)
- Lugian Launcher, Grievver Infused (Bow or XBow)
- Torment Wisp, Slash AR
- Vicious Remoran Sapper, Blunt AR
- Pike Grievver, Grievver Infused (Bow or XBow)
- Grievver Darter, Grievver Infused (Bow or XBow)
- Vibrant Shadow, Life-attuned Shadowfire Isparian Wand
- Mukkir Predator, Pierce Rend Mukkir Slayer
- Ruuk Ranger, Burun Hunters Martyr Scepter
- Guruk Grunt, Burun Hunters Martyr Scepter
- Crazed Olthoi, Paradox-touched Olthoi Bow
- Tundra Tusker, Tusker Paw Wand
- Defensive Crystal, Fire Rend
- Stinging Armoredillo, Slash AR
- Wight Sage, Fire Rend Undead Slayer
- Blade Lieutenant, Acid AR
- Blade Captain, Acid AR
- Blade Champion, Acid AR
- Tusker Heaver, Fire Rend


<div>
  <p>Archer VTank Monster Tab</p>
  <img src="Images/Archer_VTank-MonsterTab.png" width="600">
</div>


### Comments:
Archers are overall the most powerful character class on gauntlet runs and an ideal run would have around 8-9 archers.