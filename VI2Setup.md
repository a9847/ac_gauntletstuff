## Virindi Integrator 2 Setup

This guide provides instructions for setting up the Virindi Integrator 2 Plugin for use on the DrunkenFell ACE Server. Virindi Integrator 2 is essential for beating Gauntlet when using the VTank Plugin, as it enables health and monster debuff information sharing.

- Health sharing: Players can share health information between characters and fellowships, allowing them to coordinate their efforts to stay alive during combat.
- Debuff/vulnerability sharing: Players can share information about debuffs and vulnerabilities between characters and fellowships. This allows them to avoid duplicating debuffs on a monster and to prioritize attacking debuffed monsters over those that are not.

This plugin comes as part of the Virindi Tank Bundle and is installed alongside Virindi Tank.

---

The official Virindi Integrator server (white.virindi.com) no longer exists, however the plugin can be pointed towards a custom server, such as the one hosted by the DrunkenFell staff.

The following steps assume you plan to use Virindi Integrator with the DrunkenFell server. This involves modifying files within the `C:\Windows\System32\drivers\etc` directory, so it is strongly recommended to create a backup copy of the original file as detailed below:

1. Open a command prompt window (`Start Menu` > `(Type cmd.exe)` > `Hit Enter`).
2. Type the following command then hit enter:
    ```
    nslookup df.drunkenfell.com
    ```
3. Take note of the IP address displayed after the "Address:" line.
4. Browse to the folder `C:\Windows\System32\drivers\etc`.
5. Copy the file `hosts` to your desktop.
6. Create a copy of the host file on the desktop, naming the copy `hosts.bak`..
   1. you should now have two files. "hosts" and "hosts.bak"
7. On your desktop, right-click `hosts`, select `Open With`, then choose `Notepad`.
8. At the very bottom of the file, create a new blank line and insert the following, substituting the IP address you noted in step 3:
    ```
    166.164.201.19 white.virindi.net #DNF VI Server
    ```
9.  The file should now look similar to the following image.
    
    ![Updated hosts file](/Images/VI2_HostsFile.png)

10. Save the changes.
11. Move the updated `hosts` file from your desktop to `C:\Windows\System32\drivers\etc`, replacing the existing one when prompted.
12. Within Asheron's Call, click on the VI2 Plugin (looks like a frosty beer mug).
13. If the button on the bottom right-hand side says "Cancel", click it. It should then display "Log In".
     
    ![]() <img src="Images/VI2_Pre-Login.png" width="300">
     
14. Enter a unique username and password in the boxes. This should not be the same as the username and password you use to log into the game.

    ![]() <img src="Images/VI2_Initial-Login.png" width="300">
    
15. Click on "Log In". Your account will automatically be registered, similar to when you log in to AC.
16. After successfully logging in, you should see the button on the bottom right change to "Disconnect".
     
     ![]() <img src="Images/VI2_Connected.png" width="300">
     
17. You should now be logged into VI2. For a detailed list of commands and features, follow this link: http://www.virindi.net/wiki/index.php/Virindi_Integrator_2.
18. Repeat this process on each of your characters (reusing your same username and password).

---
### Disclaimer

The information in this guide is provided for educational and informational purposes only. The author of this guide is not responsible for any damages that may result from the use or misuse of the information presented.

Please note that Virindi Integrator 2 is a third-party utility that is no longer being developed and may potentially have inherent vulnerabilities. The use of this utility is at your own risk. The author of this guide is not responsible for any damages that may result from the use or misuse of Virindi Integrator 2 or any other third-party software that may violate the terms of service or end-user license agreements of Asheron's Call or any other game or software.

It is your responsibility to ensure the security of your system and the integrity of the game. We recommend that you use caution and exercise due diligence when using any third-party software or utilities, and that you adhere to the terms of service and end-user license agreements of the game or software you are using.

Furthermore, the information in this guide is provided under the terms of the MIT License, which can be found in the accompanying LICENSE file. By accessing or using the information in this guide, you agree to be bound by the terms of the MIT License.