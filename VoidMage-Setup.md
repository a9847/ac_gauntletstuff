## Asheron's Call - Gauntlet Prep Checklist - War Mage

Tips for a Successful Void Mage Character in a Gauntlet Run: Checklist of Presumed Requirements and Strong Recommendations.

Recommended Virindi Tank configuration can be found at the bottom of this document.


### Required Base:
- [ ] Level 275
- [ ] All XP Augmentations
- [ ] Fully Lum Augmented at Nalicana and Seer of Choice
- [ ] 460+ Buffed Health
- [ ] Summoning Specialized
- [ ] 35+ Damage Rating 
- [ ] 15+ Crit Damage Rating
- [ ] Full Legendary Suit, Including Legendary Life Magic
- [ ] A way of quickly burning Vit if you die 
  - [ ] Being lifestoned at DI and 10+ Corrupted Essence in inventory is the recommended method.
- [ ] Curse of Raven Fury (Tugak)
- [ ] Incantation of Marty's Hecatomb
  - [ ] Learned via Life Magic Certificate (Obtained via Stipends)

### Required items:
- [ ] Luminous Crystal of Rare Armor Damage Boost V (Or Rare Armor)
- [ ] 10+ Spectral Crystal of the Corruptor
- [ ] 10+ Spectral Crystal of the Life Giver

### Required Weapons:
- Quest Weapons
  - [ ] Tusker Paw Wand
  - [ ] Shadownether Isparian Wand
  - [ ] Burun Hunters Martyr Scepter (Requires 385 buffed Life Magic)
  - [ ] Nether-attuned Rynthid Tentacle Wand
  - [ ] Soul Bound Staff
  - [ ] Paradox-touched Olthoi Wand (Life)
  - [ ] Enhanced Assault Orb
  - [ ] Grievver Infused Nether Wand
- Tinkered (Critical Strike)
  - [ ] Mukkir Slayer 
  - [ ] Undead Slayer


### Recommended:
- [ ] 5 Adept / 4 Defender Armor Set
  - [ ] If using rare equip 5 Adept / 3 Def
- [ ] 40+ Damage Rating
- [ ] 20+ Crit Damage Rating
- [ ] Summoning Spec
- [ ] Luminous Crystal of Rare Damage Reduction V
- [ ] Burning Coal / Jaria Burning Coal
- [ ] Equipment that gives Warrior's Vitality
- [ ] 30+ Mushroom Jerky
- [ ] Tusker Stout Beer (10+ For these attributes)
  - [ ] Focus
  - [ ] Self
  - [ ] Endurance
- [ ] Luminous Crystal of Vitality
- [ ] Endless Mucor Fungus
- [ ] Endless Macran'oni and Cheese
- [ ] Hunter's Healing Salve
- [ ] Lesser Stamina Kits
- [ ] Black Luster Pearl
- [ ] MR Hunter Summon
- [ ] Honeyd Mead (Life,Vigor,Mana)
- [ ] Cave Penguin Essence
- [ ] 10+ Hunter's Mead

### VTank Settings
<div>
  <p>Void Mage VTank Options(Main) Tab</p>
  <img src="Images/VoidMage_VTank-MainTab.png" width="600">
</div>
<div>
  <p>Void Mage VTank Vitals Tab</p>
  <img src="Images/VoidMage_VTank-VitalsTab.png" width="600">
</div>
<div>
  <p>Void Mage VTank Consumables Tab</p>
  <img src="Images/VoidMage_VTank-ConsumablesTab.png" width="600">
</div>


Weapon Types for Monsters
- Corrosive Archer, Soul Bound Staff
- Virindi Rival, Nether-attuned Rynthid Tentacle Wand
- Tumerok Savage, Enhanced Assault Orb
- Lugian Launcher, Grievver Infused Nether Wand
- Torment Wisp, CS
- Vicious Remoran Sapper, CS
- Pike Grievver, Grievver Infused Nether Wand
- Grievver Darter, Grievver Infused Nether Wand
- Vibrant Shadow, Shadownether Isparian Wand
- Mukkir Predator, CS Mukkir Slayer
- Ruuk Ranger, Burun Hunters Martyr Scepter
- Guruk Grunt, Burun Hunters Martyr Scepter
- Crazed Olthoi, Paradox-touched Olthoi Wand
- Tundra Tusker, Tusker Paw Wand
- Defensive Crystal, CS
- Stinging Armoredillo, CS
- Wight Sage, CS Undead Slayer
- Blade Lieutenant, CS
- Blade Captain, CS
- Blade Champion, CS
- Tusker Heaver, Tusker Paw Wand


<div>
  <p>Void Mage VTank Monster Tab</p>
  <img src="Images/VoidMage_VTank-MonstersTab.png" width="600">
</div>


### Comments:
Void Mages do fairly well in Gauntlet. This is because they can fairly consistently damage the monsters in each of the stages, whereas there are several monsters War Mages aren't effective against due to elemental magic resistances. The downside is that their overall damage is mid-range.
